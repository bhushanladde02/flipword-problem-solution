var map = new Object();  // This is the main map where all mapping is present.

//Function Purpose : this function is helpful to add the number of buttons dynamically on HTML page 
function add(thisvalue) {
	var numberOfButtons=document.getElementById("numberofButton").value;
	if(isNaN(numberOfButtons) || numberOfButtons ==''){
		alert('Please enter the values');
		return;
	}
	if(numberOfButtons % 2 != 0){
		alert ('Please enter the even number');
		return;
	}else{
		var table = document.getElementById("tableId");
		var count=0;
		var rowCount=2;
		var row = table.insertRow(rowCount);
		for(var j=0;j<numberOfButtons;j++){
			var x = document.querySelectorAll("button");
			if(count==2){
				rowCount=(x.length/2)+1;
				var row = table.insertRow(rowCount);
				count=0;
			}
			var cell1 = row.insertCell(count);
			
			var element = document.createElement("button");
			element.value = numberOfButtons; 
			element.style = "width: 100%; height:100px;";
			element.id = "button"+(x.length+1); // And the name too?
			element.innerHTML="<span style='color: black;'><b>"+"Button"+(x.length+1)+" - On"+"</b></span>";
			element.class="btn-group";
			cell1.appendChild(element);
			element.onclick = function() { 
				button(document.getElementById(this.id));
			};
			count++;
		}
		getMapping();
	}
}


//Function Purpose : Provide one way random mapping between buttons
function getMapping(){
	var x = document.querySelectorAll("button");
	var y = document.querySelectorAll("button");
	var arrayIndexesOf_Y=new Object();
	var arrayIndexesOf_X=new Object();
	var count=0;
	while(count < x.length){
		var randomIndex=Math.floor(Math.random() * y.length);
		var rand=y[randomIndex];
		if(rand.id == x[count].id  
				|| arrayIndexesOf_Y[randomIndex] == '-99'
					|| arrayIndexesOf_X[count]=='-99'
						|| map[x[count].id]==rand.id 
						|| map[rand.id]==x[count].id){
			continue;
		}
		else{
			map[x[count].id]=rand.id;
			if (randomIndex > -1) {
				arrayIndexesOf_Y[randomIndex]='-99';
				arrayIndexesOf_X[count]='-99;';
			}
			count++;
		}

	}
}

//Function Purpose : This is the only functions for all buttons
function button(obj){
	changeState(obj);
	var associationId=map[obj.id];
	var mapObj=document.getElementById(associationId);
	changeState(mapObj);
	setTimeout(function(){
		checkWin();
	},300);	
}

//Function Purpose : This is helpful to check whether player is win the game!!
function checkWin(){
	var x = document.querySelectorAll("button");
	var checkStatus=0;
	for(var i=0;i<x.length;i++){
		if(x[i].innerHTML.indexOf("Off")== -1){
			checkStatus=1;
		}
	}
	if(checkStatus==0){
		alert("Congrats!! You win!!");
		location.reload();
	}
}

//Function Purpose : This is helpful to change the current state of button 
function changeState(obj){
	var initialVal = obj.innerHTML;
	if(initialVal.indexOf("On")!= -1){
		obj.innerHTML = obj.innerHTML.replace("On", "Off");
		obj.innerHTML = obj.innerHTML.replace("black","red");
	}
	else{
		obj.innerHTML = obj.innerHTML.replace("Off", "On");
		obj.innerHTML = obj.innerHTML.replace("red","black");
	}
}
